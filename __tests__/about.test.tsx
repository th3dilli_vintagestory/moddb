// __tests__/about.test.tsx

import { render, screen } from '@testing-library/react'
import { expect, describe, it } from '@jest/globals';
import '@testing-library/jest-dom';

import About from '../src/pages/about'

describe('About component', () => {
  it('outputs "About works"', async () => {
    const container = render(<About />);

    // usually more sophisticated, but about page does not have any business logic,
    // so just check if the module renders the headings and team members correctly
    expect(container.getAllByRole('heading')[0]?.textContent).toBe('Idea');
    expect(container.getAllByRole('heading')[1]?.textContent).toBe('Team');

    expect(container.getAllByRole('link')[0]?.textContent).toBe('@stasbern1');
    expect(container.getAllByRole('link')[1]?.textContent).toBe('@Th3Dilli');
    expect(container.getAllByRole('link')[2]?.textContent).toBe('@felixgaggl');
    expect(container.getAllByRole('link')[3]?.textContent).toBe('@fischly');


  })
})