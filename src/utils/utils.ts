import type { ModC } from "../types/Mod";

export const sortModsByName = (modss: ModC[] | undefined, ASC = false) => {//{ modss, ASC = false }) => {
    // we don't want to modify the original list of jobs provided
    if (modss) {
      const sorted = [...modss];
      sorted.sort(function (mod1, mod2) {
        if (ASC) {
          return mod2.title.localeCompare(mod1.title);
        } else {
          return mod1.title.localeCompare(mod2.title);
        }
      });
      return sorted;
    }
  };
  
  export const sortModsByDownloads = (modss: ModC[] | undefined, ASC = false) => {
    // we don't want to modify the original list of jobs provided
    if (modss) {
      const sorted = [...modss];
      sorted.sort(function (mod1, mod2) {
        if (mod1.downloads < mod2.downloads) return ASC ? -1 : 1;
        else if (mod1.downloads > mod2.downloads) return ASC ? 1 : -1;
        else return 0;
      });
      return sorted;
    }
  };
  
  export const sortModsByFollows = (modss: ModC[] | undefined, ASC = false) => {
    // we don't want to modify the original list of jobs provided
    if (modss) {
      const sorted = [...modss];
      sorted.sort(function (mod1, mod2) {
        if (mod1.follows < mod2.follows) return ASC ? -1 : 1;
        else if (mod1.follows > mod2.follows) return ASC ? 1 : -1;
        else return 0;
      });
      return sorted;
    }
  };