import type { NextPage } from "next";
import type { FileC } from "../types/File";
import UploadForm from "./components/upload-form";


const Upload: NextPage = () => {
    const onFileUploaded = (uploadedFile: FileC) => {
        console.log('[CALLBACK] NEW FILE: ', uploadedFile);
    };

    return (<>
        <UploadForm onFileUploaded={onFileUploaded}></UploadForm>
    </>);
}

export default Upload;