type Props = {
  authors: string[]
  tags: string[]
  onAuthorChange: (e: any) => void
  onTagChange: (e: any) => void
    //list of authors/tags
}

export default function ModFilters(props: Props) {
    return (
      <>
        <div className="ml-10">
          <div className='block text-sm font-sm text-gray-500 italic w-full'>Authors</div>
            <select onChange={props.onAuthorChange} className='mt-1 block text-black pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-sm'>
              <option key={"default"} value="default">-</option>
              {props.authors.map((author) => (
                                    <option key={author} value={author}>
                                        {author}
                                    </option>
                                ))}
            </select>
        </div>
        <div className="ml-10">
          <div className='block text-sm font-sm text-gray-500 italic w-full'>Tags</div>
            <select onChange={props.onTagChange} className='mt-1 block text-black pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-sm'>
              <option key={"default"} value="default">-</option>
              {props.tags.map((tag) => (
                                    <option key={tag} value={tag}>
                                        {tag}
                                    </option>
                                ))}
            </select>
        </div>
      </>
    )
  }

  export async function getServerSideProps() {
    return { props: {} };
}