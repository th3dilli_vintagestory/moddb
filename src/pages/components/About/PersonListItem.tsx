import Link from "next/link";
import { FaUserNinja, FaWrench, FaGitlab} from "react-icons/fa";

import type { Person } from "../../../types/About/Person";


type Props = {
    person: Person
}

function PersonListItem(props: Props) {
    const person = props.person;
    
    return (<>
        <div className={ "bg-slate-800 p-1 m-1 rounded-xl shadow-inner shadow-slate-900" /*border-2 border-slate-700*/ }> 
            <p className="text-xl font-semibold m-2">
                {person.firstName} {person.lastName}
                <span className="float-right">
                    <FaUserNinja />
                </span>
            </p>
            <hr className="my-2 h-px bg-gray-200 border-0 dark:bg-gray-700"></hr>
            <p className="text-lg m-2">
                <FaWrench className="inline" /> Role: &nbsp;
                { person.role }
            </p>
            <p className="text-lg m-2">
                <FaGitlab className="inline" /> Gitlab: &nbsp;
                <Link href={`https://gitlab.com/${person.gitlabTag}`} className="underline">
                    { `@${person.gitlabTag}` }
                </Link>
            </p>
        </div>
    </>);
}

export default PersonListItem;

export async function getServerSideProps() {
    return { props: { } };
}