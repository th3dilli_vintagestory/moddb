import { BsFillSuitHeartFill } from "react-icons/bs";

export default function Footer() {
    return (
        <footer className="shadow-sm bg-footer">
            <div className="flex justify-center">
                <div className="flex flex-row-3 p-5 gap-2">
                    <div>
                        <p>Made with</p>
                    </div>
                    <div className="self-center text-red-600">
                        <BsFillSuitHeartFill />
                    </div>
                    <div>
                        <p> using Nextjs</p>
                    </div>
                </div>
            </div>
        </footer>
    )
}