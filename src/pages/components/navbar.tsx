import Button from "@mui/material/Button";
import { useSession } from "next-auth/react";
import Link from "next/link";
import { BsFillHouseFill, BsPersonCircle, BsUpload, BsFillGearFill, BsInfo } from "react-icons/bs";
import { FaInfo } from "react-icons/fa";
import { FcCheckmark } from "react-icons/fc";

export default function Navbar() {
    const { data: sessionData } = useSession();
    return (
        <header className="shadow-sm bg-navbar">
            <nav className="p-4 flex justify-between ">
                <ul className="flex gap-4 mt-1.5">
                    {/* <li>
                        <Link href="/" className="font-bold">
                            <div className="flex align-center">
                                <BsFillHouseFill className="inline mr-2" size="1.3em" />
                                Home
                            </div>
                        </Link>
                    </li> */}
                    <li>
                        <Link href="/mods" className="font-bold">
                            <div className="flex align-center">
                                <BsFillGearFill className="inline mr-2" size="1.3em" />
                                Mods
                            </div>
                        </Link>
                        {/* <Link className="jus" href="">Mods</Link> */}
                    </li>
                   
                    <li>
                        <Link href="/about" className="font-bold">
                            <div className="flex align-center  ml-5">
                                <FaInfo className="inline mr-2" size="1.3em" />
                                About
                            </div>
                        </Link>
                    </li>

                    <li>
                        <Link href="/about" className="font-bold">
                            <div className="flex align-center  ml-5">
                                <FaInfo className="inline mr-2" size="1.3em" />
                                Helllo
                            </div>
                        </Link>
                    </li>


                </ul>
                <ul className="flex gap-4">
                    <li>
                        <Link href="/createMod" className="font-bold">
                            <Button sx={{backgroundColor: '#1565c0 !important'}} variant="contained" endIcon={<BsUpload />}>
                                Upload a mod
                            </Button>
                            </Link>
                        {/* <Link className="" href="/createMod"><BsUpload/></Link> */}
                    </li>
                    <li>
                        <Link href="/account">
                            {!sessionData && <BsPersonCircle size="2em" />}
                            {sessionData && <img src={sessionData?.user?.image || "" } width="30"/>}
                        </Link>
                    </li>
                </ul>
            </nav>
        </header>
    )
}