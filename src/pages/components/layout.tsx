import { ReactNode } from "react";
import Footer from "./footer";
import Navbar from "./navbar";

type Props = {
    children: ReactNode
}

export default function Layout(props: Props) {
    return (
        <>
            <Navbar />
            <main>{props.children}</main>
            <Footer />
        </>
    )
}