import React from "react";

import { BsDownload, BsFilePersonFill, BsFillSuitHeartFill, BsFillPencilFill, BsFillTrashFill } from "react-icons/bs";
import { MdUpdate, MdOutlineMoreTime } from "react-icons/md";

import ImageGallery from 'react-image-gallery';
import 'react-image-gallery/styles/css/image-gallery.css';

/* Markdown formatting */
import CustomMarkdown from "./Util/CustomMarkdown";

import type { ModC } from "../../../types/Mod";
import CommentSection from "./CommentSection/CommentSection";
import FollowButton from "./Util/FollowButton";

/* TimeAgo */
import TimeAgo from "javascript-time-ago";
import en from 'javascript-time-ago/locale/en.json';
import de from 'javascript-time-ago/locale/en.json';
TimeAgo.addDefaultLocale(en);
TimeAgo.addLocale(de);
import ReactTimeAgo from 'react-time-ago';
import ReleaseList from "./ReleaseList";

/* Custom module style */
import style from './ModDetails.module.css';

import logo from '../../../../public/images/logo.png'
import Link from "next/link";
import { trpc } from "../../../utils/trpc";
import { useRouter } from "next/router";

import { toast } from "react-toastify";
import Button from "@mui/material/Button";

type Props = {
    mod: ModC
    onContentChanged?: () => void
}


const ModDetails = (props: Props) => {
    const mod = props.mod;
    
    const { mutate: deleteModQuery } = trpc.mods.deleteMod.useMutation({
        onSuccess() {
            router.push( { pathname: '/mods', query: { pid: mod.id }});
        },
        onError(data) {
            toast(data.message);
        }
    });
    const router = useRouter();

    const deleteMod = async () => {
        deleteModQuery(mod.id);
    };

    return (<>
        <div className="card bg-card m-5 px-8">
            <div className="grid grid-cols-2 gap-10">
                <div className="flex justify-center my-7">
                    <ImageGallery items={mod.images.slice(1).map(i => { return { original: i.url, thumbnail: i.url, originalClass: style.gallery_croppedImage }})} showPlayButton={false} lazyLoad={true} additionalClass={style.gallery_root}></ImageGallery>
                </div>
                <div>
                    <div className="relative top-0 right-0 float-right px-2 cursor-pointer" > <BsFillTrashFill onClick={() => deleteMod()}/></div>
                    <Link className="relative top-0 right-0 float-right" href={"/editMod/" + mod.id}> <BsFillPencilFill /></Link>
                    <h2 className="text-4xl my-7">{mod.title}</h2>
                    <div className="grid grid-cols-2">
                        <div>
                            <div className="grid gap-y-1 grid-cols-[30px_33%_66%]">
                                <div className="self-center"><BsFilePersonFill /></div>
                                <div><span className="font-bold">Author</span>: </div>
                                <div>{mod.author}</div>

                                <div className="self-center"><BsDownload /></div>
                                <div><span className="font-bold">Downloads</span>:</div>
                                <div>{mod.downloads}</div>

                                <div className="self-center"><BsFillSuitHeartFill /></div>
                                <div><span className="font-bold">Follower</span>: </div>
                                <div>{mod.follows}</div>
                            </div>
                            <div className="mt-5 grid gap-y-1 grid-cols-[30px_33%_66%]">
                                <div className="self-center"><MdOutlineMoreTime /></div>
                                <div><span className="font-bold">Created at</span>: </div>
                                <div>{mod.createdAt && (<ReactTimeAgo date={mod.createdAt}></ReactTimeAgo>)}</div>

                                <div className="self-center"><MdUpdate /></div>
                                <div><span className="font-bold">Updated at</span>: </div>
                                <div>{mod.updatedAt && (<ReactTimeAgo date={mod.updatedAt}></ReactTimeAgo>)}</div>
                            </div>

                            <div className="my-4">
                               <FollowButton mod={mod} onContentChanged={props.onContentChanged}></FollowButton>
                               {mod.Assets && mod.Assets.length > 0 && (
                                <div className="ml-5 inline">
                                    <Link href={mod.Assets[0]!.url!}>
                                        <Button sx={{backgroundColor: '#1565c0 !important'}} variant="contained" endIcon={<BsDownload />}>Download</Button>
                                    </Link>
                                </div>
                               )}
                            </div>
                        </div>
                        <div>
                            
                            <div className="flex flex-wrap">
                                {mod.tags.map((tag, index) => (
                                    <div key={index} className="tag bg-indigo-600 rounded m-1 p-1">
                                        #{tag}
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>

                    <h1 className="text-2xl mt-5">Description</h1>
                    <hr className="my-2"></hr>

                    <CustomMarkdown content={mod.description}></CustomMarkdown>
                </div>
            </div>
            {/* <h1 className="text-2xl mt-5">Releases</h1>
            <hr className="my-2"></hr>
            <ReleaseList releases={mod.releases!}></ReleaseList> */}

            <h1 className="text-2xl mt-5">Comments</h1>
            <hr className="my-2"></hr>
            {mod.comments != null &&
                <CommentSection mod={mod} onCommentAdded={props.onContentChanged}></CommentSection>
            }
        </div>
    </>);
}


export default ModDetails;


export async function getServerSideProps() {
    return { props: {} };
}