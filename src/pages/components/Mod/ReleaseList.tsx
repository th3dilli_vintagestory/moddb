import React from "react";

import { BsDownload, BsFilePersonFill, BsFillSuitHeartFill } from "react-icons/bs";
import { MdUpdate, MdOutlineMoreTime } from "react-icons/md";

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TablePagination from '@mui/material/TablePagination';
import Paper from '@mui/material/Paper';

import type { ReleaseC } from "../../../types/Release";
import FollowButton from "./Util/FollowButton";

/* TimeAgo */
import TimeAgo from "javascript-time-ago";
import en from 'javascript-time-ago/locale/en.json';
import de from 'javascript-time-ago/locale/en.json';
TimeAgo.addDefaultLocale(en);
TimeAgo.addLocale(de);
import ReactTimeAgo from 'react-time-ago';
import { Button } from "@mui/material";


type Props = {
    releases: ReleaseC[]
}

interface Column {
    id: 'version' | 'downloads' | 'releasedate' |  'download' | 'changelog'
    label: string;
    minWidth?: number;
    align?: 'right';
    format?: (value: number) => string;
  }
  
  const columns: readonly Column[] = [
    { id: 'version', label: 'Version', minWidth: 170 },
    { id: 'downloads', label: 'Downloads', minWidth: 100 },
    { id: 'releasedate', label: 'Release date', minWidth: 170 },
    { id: 'changelog', label: 'Changelog', minWidth: 170 },
    { id: 'download', label: '', minWidth: 170 }
  ];


const ReleaseList = (props: Props) => {    
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(3);
  
    const handleChangePage = (event: unknown, newPage: number) => {
      setPage(newPage);
    };
  
    const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
      setRowsPerPage(+event.target.value);
      setPage(0);
    };
  
    return (
    <div className="mx-4 mt-4">
      <Paper sx={{ width: '100%', overflow: 'hidden' }}>
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    <span className="font-bold">{column.label}</span>
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {props.releases
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((release) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={release.id}>
                        <TableCell>{release.version}</TableCell>
                        <TableCell>{release.downloads}</TableCell>
                        <TableCell>{release.createdAt?.toLocaleDateString()}</TableCell>
                        <TableCell>{release.changelog.slice(0, 42) + (release.changelog.length > 42 ? '...' : '')}</TableCell>
                        <TableCell align="right">
                            <Button variant="outlined">Download</Button>
                        </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[3, 10, 20]}
          component="div"
          count={props.releases.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
      </div>
    );
}


export default ReleaseList;


export async function getServerSideProps() {
    return { props: {} };
}