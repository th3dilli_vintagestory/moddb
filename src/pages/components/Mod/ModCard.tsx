import Link from "next/link";
import { BsDownload, BsFilePersonFill, BsFillSuitHeartFill } from "react-icons/bs";
import type { ModC } from "../../../types/Mod";
import logo from '../../../../public/images/logo.png'
import Image from 'next/image'
import commaNumber from  'comma-number';

import style from './ModCard.module.css';


type Props = {
    mod: ModC
}

function ModCard(props: Props) {
    const mod = props.mod;
    return (<>
        <div className="card bg-card">
            <Link href={"/mod/" + mod.id}>
                <p className="font-bold text-center truncate">{mod.title}</p>
                <div className="mt-2"></div>
                <hr />
                <div className="mt-2"></div>
                <div className="flex justify-center">
                   {/* <Image src={logo} alt={""} width={100} /> */}
                   <img src={mod.images.find(img => img.fileName == '_logo.png')?.url ?? '/images/logo.png' } alt={""} className={style.logo} />

                </div>
                {/* <p className="m-1 line-clamp-2">{mod.description}</p> */}
                <div className="pl-2 mt-4 mb-2">
                    <div className="grid gap-y-1 grid-cols-[30px_100%]">
                        <div className="self-center"><BsFilePersonFill /></div>
                        <div><span className="font-bold">Author</span>: {mod.author}</div>

                        <div className="self-center"><BsDownload /></div>
                        <div><span className="font-bold">Downloads</span>: {commaNumber(mod.downloads, '.')}</div>

                        <div className="self-center"><BsFillSuitHeartFill /></div>
                        <div><span className="font-bold">Follower</span>: {commaNumber(mod.follows, '.')}</div>
                    </div>
                </div>
                <div className="grid grid-cols-auto mt-1">
                    {mod.tags.slice(0, 2).map((tag, index) => (
                        <div key={index} className="tag bg-indigo-600 rounded m-1 p-1">
                            {`#${tag}`}
                        </div>
                    ))}
                </div>
            </Link>
        </div>
    </>);
}

export default ModCard;

export async function getServerSideProps() {
    return { props: {} };
}