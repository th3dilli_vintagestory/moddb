import React from "react";
import Image from "next/image";

import CustomMarkdown from "../Util/CustomMarkdown";
import type { CommentC } from "../../../../types/Comment";

import TimeAgo from "javascript-time-ago";
import en from 'javascript-time-ago/locale/en.json';
import de from 'javascript-time-ago/locale/en.json';

TimeAgo.addDefaultLocale(en);
TimeAgo.addLocale(de);

import ReactTimeAgo from 'react-time-ago';
import { useSession } from "next-auth/react";


type Props = {
    comment: CommentC
}


const Comment = (props: Props) => {
    const { data: sessionData } = useSession();
    
    const comment = props.comment;

    return (<>
        <div className="card bg-slate-800 my-5 mx-3">
            <p>

                <img src={comment.Author.image ? comment.Author.image : `https://ui-avatars.com/api/name=${comment.Author.name}&background=random`} alt={''} width={50} height={50} className="rounded-3xl inline m-1 mr-4" />
                <span className="font-bold mr-3">{comment.Author.name}</span>
                { comment.postedAt != null && 
                    <span><ReactTimeAgo date={comment.postedAt} className="inline font-light text-slate-300"></ReactTimeAgo></span>
                }
            </p>
            <p className="m-3">
                <CustomMarkdown content={comment.content}></CustomMarkdown>
            </p>
        </div>
    </>);
}


export default Comment;


export async function getServerSideProps() {
    return { props: {} };
}