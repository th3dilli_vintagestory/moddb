import React, { useMemo, useState } from "react";
import ReactDOMServer from "react-dom/server";
import dynamic from "next/dynamic";
import { trpc } from "../../../../utils/trpc";

import Button from '@mui/material/Button';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import { FcExpand } from "react-icons/fc";
import { AiOutlineInfoCircle } from "react-icons/ai";

import CustomMarkdown from "../Util/CustomMarkdown";

import { toast } from 'react-toastify';

/* Markdown editor */
import "easymde/dist/easymde.min.css";
const SimpleMdeEditor = dynamic(
	() => import('react-simplemde-editor'),
	{ ssr: false }
);

import type { ModC } from "../../../../types/Mod";
import Comment from "./Comment";
import UploadForm from "../../upload-form";
import type { FileC } from "../../../../types/File";

type Props = {
    mod: ModC
    onCommentAdded?: () => void
}


const CommentSection = (props: Props) => {
    const mod = props.mod;
    const comments = mod.comments;

    const [tempImages, setTempImages] = useState<FileC[]>([]);
    const onFileUploaded = (newfile: FileC) => {
        tempImages.push(newfile);
        setTempImages(tempImages.slice());

        console.log('UPDATET TEMPIMAGES: ', tempImages);
    };

    const { mutate: addComment } = trpc.comments.addComment.useMutation({
        onSuccess() {
            // call callback, if it was set
            if (props.onCommentAdded)
                props.onCommentAdded();
            // clear editor
            setEditorValue('');
        },
        onError(data) {
            console.log('[ONERROR] data = ', data);
            if (data.data?.code == 'UNAUTHORIZED') {
                // TODO: redirect to login
            }
        }
    });

    const [editorValue, setEditorValue] = useState('');
    const onEditorChange = (value: string) => setEditorValue(value);

    const handlePostButtonClick = () => {
        // finalize the previously uploaded images
        fetch('/api/finalize', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(tempImages)
        })
        .then(res => res.json())
        .then(json => {
            if (json.error) {
                toast(json.error);
                console.error('error with route: ', json.error.text);
            } else {
                // replace the blob url's (which are just locally in the browser),
                // by the URLs after the actual upload.
                // TODO: check URLs on server side (even though, by default, no
                // external links are allowed anyway).
                let commentText = editorValue;
                const urlsFound = (/(blob:[^"\)]+)/g).exec(commentText);

                const finalizedImages: FileC[] = json;
                if (urlsFound) {
                    for (const url of urlsFound) {
                        const matchingImage = finalizedImages.find(i => i.dataURL == url);

                        if (matchingImage && matchingImage.publicUri) {
                            commentText = commentText.replace(url, matchingImage.publicUri);
                        }
                    }
                }

                // make request to add the comment
                addComment({
                    modid: mod.id,
                    content: commentText
                });
            }
        }).catch(err => toast(err));

    };

    const editorOptions = useMemo(() => {
        return {
            previewRender(plainText: any) {
                return ReactDOMServer.renderToString(<CustomMarkdown content={plainText} spoilersExpanded={true}></CustomMarkdown>);
            }
        };
    }, []);

    return (<>
        {comments != null && comments.map((comment) => (
            <Comment key={comment.id} comment={comment}></Comment>
        ))}
        {(comments == null || comments.length <= 0) && (
            <p className="mx-4 my-4">No comments yet.</p>
        )}

        <div className="mx-4">
            <Accordion>
                <AccordionSummary expandIcon={<FcExpand />} aria-controls="panel1a-content" id="panel1a-header">
                    <span className="text-2xl">Comment</span>
                </AccordionSummary>
                <AccordionDetails>
                    <SimpleMdeEditor value={editorValue} onChange={onEditorChange} options={editorOptions}></SimpleMdeEditor>
                    <div className="-mt-3">
                        <span className="text-slate-400"> <AiOutlineInfoCircle className="inline align-text-bottom" /> First upload an image. To insert it, click on the thumbnail that appears below.</span>
                        <UploadForm onFileUploaded={onFileUploaded} shouldReturnDataURL={true}></UploadForm>
                    </div>
                    <div className="flex float-left">
                        {tempImages.map((image) => {
                            return (<>
                                <div className="flex-auto mt-4 mr-3" key={image.newFilename} onClick={() => {setEditorValue(editorValue + `![${image.originalFilename}](${image.dataURL})`)}}>
                                    {image.dataURL && (
                                        <img src={image.dataURL} className="h-[100px] border border-slate-600 shadow-md" alt={image.originalFilename}></img>
                                    )}
                                    <p className="text-center">{image.originalFilename}</p>
                                </div>                               
                            </>);
                        })}
                    </div>
                    <div className="text-right">
                        <Button onClick={handlePostButtonClick} variant="contained" className="" sx={{backgroundColor: '#1565c0 !important'}}>Post comment</Button>
                    </div>
                </AccordionDetails>
            </Accordion>
        </div>
    </>);
    
}


export default CommentSection;


export async function getServerSideProps() {
    return { props: {} };
}