import Link from 'next/link';
import React from 'react';
import SimpleMarkdown from 'simple-markdown';
import style from './CustomMarkdown.module.css';

import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

type Props = {
    // the content formatted in markdown
    content: string
    spoilersExpanded?: boolean
}

/**
 * Component wrapper around SimpleMarkdown with custom styling. 
 */
function CustomMarkdown(props: Props) {
    // to customize the markdown rendering, override the react-render functions of some of the predefined rules.
    // additionally, introduce a new rule for spoilers, rendered using the MUI accordion component.
    const rules = {
        ...SimpleMarkdown.defaultRules,

        list: {
            ...SimpleMarkdown.defaultRules.list,

            react: (node: any, output: any, state: any) => {
                const getListChildren = () => node.items.map((item: SimpleMarkdown.ASTNode, i: number) => (<li key={''+i}>{output(item, state)}</li>));

                if (node.ordered) {
                    return (<ol key={state.key} start={node.start} className={style.md_ol}>{getListChildren()}</ol>);
                } else {
                    return (<ul key={state.key} className={style.md_ul}>{getListChildren()}</ul>);
                }
            }
        },

        heading: {
            ...SimpleMarkdown.defaultRules.heading,

            react: (node: any, output: any, state: any) => {
                const headingTag = 'h' + node.level;
                const headingClassName = style['md_' + headingTag];

                return React.createElement(headingTag, { key: state.key, className: headingClassName }, output(node.content, state));
            }
        },

        link: {
            ...SimpleMarkdown.defaultRules.link,

            react: (node: any) => {
                const href = SimpleMarkdown.sanitizeUrl(node.target);
                return (<Link href={href ? href : ''} className={style.md_a}>{href}</Link>)
            }
        },

        spoiler: {
            order: 10, //SimpleMarkdown.defaultRules.em.order - 0.5,

            match: function(source: any) {
                return /^\|\|([\s\S]+?)\|\|([\s\S]+?)\|\|(?!\|)/.exec(source);
            },
            parse: function(capture: any, parse: any, state: any) {
                return { content: {
                    title: parse(capture[1], state),
                    detail: parse(capture[2], state)
                } };
            },
            react: function(node: any, output: any, state: any) {
                return (
                <div className="my-3">
                    <Accordion defaultExpanded={props.spoilersExpanded} sx={{ backgroundColor: 'rgb(30 41 59 / var(--tw-bg-opacity))', color: 'white' }} className="">
                        <AccordionSummary className="font-bold" expandIcon={<ExpandMoreIcon className="text-slate-500" />}>{output(node.content.title, state)}</AccordionSummary>
                        <AccordionDetails>{output(node.content.detail)}</AccordionDetails>
                    </Accordion>
                </div>);
            }
        }
    };

    const parser = SimpleMarkdown.parserFor(rules);
    const reactOutput = SimpleMarkdown.outputFor(rules, 'react');

    const parseTree = parser(props.content + '\n\n');
    const output = reactOutput(parseTree);
    
    return (<>
        {output}
    </>);
}

export default CustomMarkdown;

export async function getServerSideProps() {
    return { props: {} };
}