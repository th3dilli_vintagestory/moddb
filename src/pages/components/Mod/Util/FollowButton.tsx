import React from "react";
import { trpc } from "../../../../utils/trpc";

import { AiOutlineStar, AiFillStar} from "react-icons/ai";
import Button from '@mui/material/Button';

/* Markdown formatting */
import type { ModC } from "../../../../types/Mod";
import { toast } from "react-toastify";
import { Follow } from "@prisma/client";


type Props = {
    mod: ModC
    onContentChanged?: () => void
}

const FollowButton = (props: Props) => {
    const { data: follow, refetch: reloadFollow } = trpc.mods.getFollowOfCurrentUser.useQuery(props.mod.id);


    const errorHandler = (data: any) => {
        if (data.data?.code == 'UNAUTHORIZED') {
            toast('Please login to follow this mod.');
        } else {
            if (data.data?.code)  {
                toast(data.data?.code);
            } else {
                toast('Error processing the follow request.');
            }
        }
    };
    const successHandler = (data: Follow) => {
        reloadFollow();
        props.onContentChanged!();
    };

    const { mutate: addFollow } = trpc.mods.addFollow.useMutation({
        onSuccess(data) { successHandler(data); },
        onError(data) { errorHandler(data); }
    });
    const { mutate: removeFollow } = trpc.mods.removeFollow.useMutation({
        onSuccess(data) { successHandler(data); },
        onError(data) { errorHandler(data); }
    });

    const handleOnFollowClick = () => {
        addFollow(props.mod.id);
    };
    const handleOnUnfollowClick = () => {
        removeFollow(props.mod.id);
    };

    return (<>
        {(follow != undefined && follow != null) && (
            <Button onClick={handleOnUnfollowClick} variant="contained" sx={{backgroundColor: '#1565c0 !important'}} ><AiFillStar className="mr-2"/> Unfollow</Button>                                    
        )}
        {(follow == undefined || follow == null) && (
            <Button onClick={handleOnFollowClick} variant="contained" sx={{backgroundColor: '#1565c0 !important'}}><AiOutlineStar className="mr-2"/> Follow</Button>
        )}
    </>);
}


export default FollowButton;


export async function getServerSideProps() {
    return { props: {} };
}