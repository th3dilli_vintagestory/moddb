import React, { useRef } from "react";
import Button from '@mui/material/Button';
import type { FileC } from "../../types/File";
import { toast } from 'react-toastify';

type UploadFormProps = {
    onFileUploaded: (file: FileC) => void
    shouldReturnDataURL?: boolean
}

const UploadForm = (props: UploadFormProps) => {
    const formRef = useRef<HTMLFormElement>(null);
    const fileInputRef = useRef<HTMLInputElement>(null);

    const handleSubmit = (e: any) => {
        // check if a file has actually been provided
        if (!formRef.current || !fileInputRef.current || !fileInputRef.current.files) { return e.preventDefault(); }
        const file = fileInputRef.current.files[0];
        if (!file) { return e.preventDefault(); }

        // build form data from the html form element
        const data = new FormData(formRef.current);
        // make api call with the form (including the file) as request body
        fetch('/api/upload', {
            method: 'POST',
            body: data
        })
        .then(res => res.json())
        .then(json => {
            if (json.error) {
                toast(json.error);
                console.error('error with route: ', json.error.text);
            } else {
                if (props.shouldReturnDataURL) {
                    extractDataURL(file).then(url => {
                        props.onFileUploaded({...json.file, dataURL: url });
                    }).catch(() => {
                        props.onFileUploaded(json.file);
                    });
                } else {
                    props.onFileUploaded(json.file);
                }

                fileInputRef.current!.value = '';
            }
        }).catch(err => toast(err));

        // prevent the default submit action, which would redirect us (refreshing the page), which we don't want
        e.preventDefault();
    };
    return (<>
        <form className="mt-2" method="post" action="/api/upload" encType="multipart/form-data" onSubmit={handleSubmit} ref={formRef}>
            <input name="file" type="file" accept=".png,.jpg,.jpeg,.gif, image/*" ref={fileInputRef} />
            <Button variant="outlined" type="submit" className="">Upload</Button>
        </form>
    </>);
};

function extractDataURL(file: any) {
    // return the data url of the selected file
    return new Promise((resolve) => {
        // const reader = new FileReader();
        // reader.onload = (e) => {
        //     resolve(e.target?.result);
        // };
        // reader.readAsDataURL(file);
        resolve(window.URL.createObjectURL(file));        
    });    
}

export default UploadForm;