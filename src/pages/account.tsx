import { type NextPage } from "next";
import { signIn, signOut, useSession } from "next-auth/react";
import Link from "next/link";

import { trpc } from "../utils/trpc";

import style from './account.module.css';


const Account: NextPage = () => {

    const { data: sessionData } = useSession();

    const { data: follows } = trpc.follows.getAllOfCurrentUser.useQuery();

    return (
        <div className="flex flex-col items-center justify-center gap-4 mt-4">
            <p className="text-center text-2xl text-white">
                {sessionData && <div>
                    <span>Logged in as {sessionData.user?.name} </span><br/>
                    {/* <span>{sessionData.user?.email}</span><br/> */}
                </div> }
            </p>
            <button
                className="mb-5 rounded-full bg-white/10 px-10 py-3 font-semibold text-white no-underline transition hover:bg-white/20"
                onClick={sessionData ? () => signOut() : () => signIn()}
            >
                {sessionData ? "Sign out" : "Sign in"}
            </button>

            <h1 className="text-2xl">Your follows:</h1>

            <ul>
            {follows?.map((follow, idx) => (
                <>
                    <li key={follow.modid} className="mb-7">
                        <Link href={`/mod/${follow.Mod.id}`}>
                        <div className="text-center">
                                {follow.Mod.title}
                            </div>
                            <img src={follow.Mod.Images.find(img => img.fileName == '_logo.png')?.url ?? '/images/logo.png' } alt={""} className={style.logo} />

                        </Link>
                    </li>
                </>
            ))}
            </ul>
        </div>
    );
};

export default Account;
