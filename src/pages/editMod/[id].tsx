import { trpc } from "../../utils/trpc";
import { useRouter } from "next/router";
import { ModC } from "../../types/Mod";
import ModDetails from "../components/Mod/ModDetails";
import { useCallback, useEffect, useState } from "react";
import { title } from "process";
import { TagsInput } from "react-tag-input-component";
import logo from '../../../public/images/logo.png'
import Image from 'next/image'

export default function Mod() {
    const router = useRouter();
    const id = router.query.id!.toString();

    const updateQuery = trpc.mods.updateMod.useMutation();


    const [message, setMessage] = useState<string>();

    const [files, setFile] = useState<File[]>([]);
    const [addedFiles, setAddedFile] = useState<File[]>([]);

    const [state, setState] = useState({
        title: "",
        description: "",
        mod_filename: "",
        id: ""
    });
    const [isLoading, setLoading] = useState(false);

    const [imageMessage, setImageMessage] = useState<string>();

    const [images, setImages] = useState<File[]>([]);
    const [imagesData, setImagesData] = useState([{
        data: "",
        name: "",
        contentType: "",
    }]);

    const [addedImages, setAddedImages] = useState<File[]>([]);
    const [addedImagesData, setAddedImagesData] = useState([{
        data: "",
        name: "",
        contentType: "",
    }]);

    const [tags, setTags] = useState<string[]>([]);

    function handleChange(e: { target: { files: any; name: any; value: any; }; }) {
        if (e.target.files) {
            setState({ ...state, mod_filename: e.target.files[0].name });

        } else {
            setState({ ...state, [e.target.name]: e.target.value });
        }
    }

    const handleFile = (e: { preventDefault: any, target: { files: any; }; }) => {

        setMessage("");
        const file = e.target.files;

        for (let i = 0; i < file.length; i++) {
            const fileType = file[i]['type'];
            const validImageTypes = ['application/x-zip-compressed', 'application/zip', 'application/zip-compressed'];
            if (validImageTypes.includes(fileType)) {
                setAddedFile([...addedFiles, file[i]]);
            } else {
                setMessage("Please upload a ZIP File");
            }

        }



    };


    async function handleSubmit(e: { preventDefault: () => void; }) {
        e.preventDefault();

        // convert data file upload into base64 format and send to server
        const reader = new FileReader();
        if ((files[0] || addedFiles[0]) && (imagesData[0] || addedImagesData[0])) {
            const fileToSend = files[0] ? files[0] : addedFiles[0];

            reader.readAsDataURL(fileToSend!);
            reader.onload = async function () {
                updateQuery.mutateAsync({ title: state.title, description: state.description, modFile: reader.result, mod_filename: fileToSend!.name, tags: tags, images: [...imagesData, ...addedImagesData], id: state.id });
                router.push(`/mods/`);
            };
            reader.onerror = function (error) {
                console.log(error);
            };
        } else if ((!files[0] && !addedFiles[0]) && (imagesData[0] || addedImagesData[0])) {
            await updateQuery.mutateAsync({ title: state.title, description: state.description, tags: tags, mod_filename: "", images: [...imagesData, ...addedImagesData], id: state.id });
            router.push(`/mods/`);
        } else if ((files[0] || addedFiles[0]) && !imagesData[0]) {
            const fileToSend = files[0] ? files[0] : addedFiles[0];

            reader.readAsDataURL(fileToSend!);
            reader.onload = async function () {
                updateQuery.mutateAsync({ title: state.title, description: state.description, modFile: reader.result, mod_filename: fileToSend!.name, tags: tags, images: [], id: state.id });
                router.push(`/mods/`);
            };
            reader.onerror = function (error) {
                console.log(error)
            };
        } else {
            await updateQuery.mutateAsync({
                title: state.title, description: state.description, tags: tags, mod_filename: "", images: [], id: state.id
            });
            router.push(`/mods/`);
        }


        setAddedImages([]);
        setAddedImagesData([]);
        setAddedFile([]);
    }



    const removeFile = (i: string) => {
        setFile(files.filter(x => x.name !== i));
        setAddedFile(addedFiles.filter(x => x.name !== i));
    }

    const removeImage = (i: string) => {
        setImages(images.filter(x => x.name !== i));
        setImagesData(imagesData.filter(x => x.name !== i));

        setAddedImages(addedImages.filter(x => x.name !== i));
        setAddedImagesData(addedImagesData.filter(x => x.name !== i));
    }


    const handleImage = (e: { target: { files: any; }; }) => {
        setImageMessage("");
        const file = e.target.files;
        for (let i = 0; i < file.length; i++) {
            const fileType = file[i]['type'];
            const validImageTypes = ['image/png', 'image/jpeg'];
            if (validImageTypes.includes(fileType)) {
                setAddedImages([...addedImages, file[i]]);
                const reader = new FileReader();
                reader.readAsDataURL(file[i]);
                reader.onload = async function () {
                    if (typeof (reader.result) === "string") {
                        setAddedImagesData([...addedImagesData, { data: reader.result, name: file[i].name, contentType: fileType }]);
                    }

                }
            } else {
                setImageMessage("Please upload an Image");
            }
        }
    };


    function getFileFromBase64(string64: string, fileName: string) {
        const trimmedString = string64.replace('dataimage/jpegbase64', '');
        const imageContent = atob(trimmedString);
        const buffer = new ArrayBuffer(imageContent.length);
        const view = new Uint8Array(buffer);

        for (let n = 0; n < imageContent.length; n++) {
            view[n] = imageContent.charCodeAt(n);
        }
        const type = 'application/x-zip-compressed';
        const blob = new Blob([buffer], { type });
        return new File([blob], fileName, { lastModified: new Date().getTime(), type });
    }

    function getImageFromBase64(string64: string, fileName: string, fileType: string) {
        const trimmedString = string64.replace('dataimage/jpegbase64', '');
        const imageContent = atob(trimmedString);
        const buffer = new ArrayBuffer(imageContent.length);
        const view = new Uint8Array(buffer);

        for (let n = 0; n < imageContent.length; n++) {
            view[n] = imageContent.charCodeAt(n);
        }
        const type = fileType;
        const blob = new Blob([buffer], { type });
        return new File([blob], fileName, { lastModified: new Date().getTime(), type });
    }


    let modChanged = "";
    let changeCounter = 0;

    const modRequest = trpc.mods.getByIdWithFiles.useQuery(id).data;
    const mod = modRequest ?? { mod: null, data: '', imageArray: [] };

    console.log(modRequest);

    if (mod != null && mod.mod != undefined && typeof(mod.mod) !== "string" && !Array.isArray(mod.mod))  {
        modChanged = mod.mod.title! + " " + changeCounter++;
    }

    useEffect(() => {
        if (mod.mod) {
            const newFiles: File[] = [];
            if (mod.mod.ModHasAssets.length > 0 && mod.data && !Array.isArray(mod.data) && typeof(mod.data === "string")) {
                setState({ ...state, mod_filename: mod.mod.ModHasAssets[0]!.Asset.filelocation, title: mod.mod.title, description: mod.mod.description, id: mod.mod.id });
                const decodedFile = getFileFromBase64((mod.data).toString(), mod.mod.ModHasAssets[0]!.Asset.filelocation);
                newFiles.push(decodedFile);

            } else {
                setState({ ...state, mod_filename: "", title: mod.mod.title, description: mod.mod.description, id: mod.mod.id });
            }
            setFile(newFiles);

            const newImages: File[] = [];
            const newImagesData = [];
            if (mod.imageArray && Array.isArray(mod.imageArray) && mod.imageArray.length > 0 && mod.mod) {
                for (let index = 0; index < mod.imageArray.length; index++) {
                    const element = mod.imageArray[index];
                    if(element && mod.mod && mod.mod.Images.length > 0){
                        const decodedImage = getImageFromBase64(element, mod.mod.Images[index]!.fileName!.toString() || "", mod.mod.Images[index]!.contentType!);
                        newImages.push(decodedImage);
                        newImagesData.push({ data: "data:" + mod.mod.Images[index]!.contentType! + "," + element, name: mod.mod.Images[index]!.fileName!, contentType: mod.mod.Images[index]!.contentType || ""})
                    }
                }
            }
            setImages(newImages);
            setImagesData(newImagesData);
            if (mod.mod.ModHasTags.length > 0) {
                const tagArray = [];
                for (let index = 0; index < mod.mod.ModHasTags.length; index++) {
                    const element = mod.mod.ModHasTags[index]!.Tag.name;
                    tagArray.push(element)
                }
                setTags(tagArray);
            }

        } else {

        }

    }, [mod])





        if (mod) {
            if (isLoading) return <p>Loading...</p>

    if (typeof id === "string") {
            return (<>
                <div className="card bg-card silver">


                    <form onSubmit={(e) => handleSubmit(e)} className="flex flex-col justify-between form-control" autoComplete="off">
                        <div >
                            <input type="text" name="title" value={state.title} onChange={(e) => handleChange(e)} placeholder="Title" id="titleInput" className="bg-black h-10 rounded-md outline-0 pl-4 w-96" required />
                            <br />
                            <br />
                            <input type="text" name="description" value={state.description} onChange={(e) => handleChange(e)} placeholder="Description" id="descriptionInput" className="bg-black h-10 rounded-md outline-0 pl-4 w-96" required />
                            <br />
                            <br />


                            <div className="p-3 md:w-1/2 w-[300px] rounded-md">
                                <span className="flex justify-center items-center bg-white text-[12px] mb-1 text-red-500">{message}</span>
                                <div className="h-32 w-full overflow-hidden relative shadow-md border-2 items-center rounded-md cursor-pointer   border-gray-400 border-dotted">
                                    <input type="file" onChange={handleFile} className="h-full w-full opacity-0 z-10 absolute" name="files[]" />
                                    <div className="h-full w-full bg-gray-200 absolute z-1 flex justify-center items-center top-0">
                                        <div className="flex flex-col">
                                            <i className="mdi mdi-folder-open text-[30px] text-gray-400 text-center"></i>
                                            <span className="text-[12px] text-black">{`Drag and Drop a file`}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex flex-wrap gap-2 mt-2">
                                    {files.map((file, key) => {
                                        return (

                                            <div key={key} className='w-full h-16 flex items-center justify-between rounded p-3 bg-white'>
                                                <div className="flex flex-row items-center gap-2">
                                                    <div className="h-12 w-12 ">
                                                        <div className="w-full h-full rounded">
                                                            <Image src={logo} alt={""} />
                                                        </div>
                                                    </div>
                                                    <span className="truncate w-44 text-black">{file.name}</span>
                                                </div>
                                                <div onClick={() => { removeFile(file.name) }} className="h-6 w-6 bg-red-400 flex items-center cursor-pointer justify-center rounded-sm">
                                                    <i className="mdi mdi-trash-can text-white text-[14px]"></i>
                                                </div>
                                            </div>

                                        )
                                    })}

                                    {addedFiles.map((file, key) => {
                                        return (

                                            <div key={key} className='w-full h-16 flex items-center justify-between rounded p-3 bg-white'>
                                                <div className="flex flex-row items-center gap-2">
                                                    <div className="h-12 w-12 ">
                                                        <div className="w-full h-full rounded">
                                                            <Image src={logo} alt={""} />
                                                        </div>
                                                    </div>
                                                    <span className="truncate w-44 text-black">{file.name}</span>
                                                </div>
                                                <div onClick={() => { removeFile(file.name) }} className="h-6 w-6 bg-red-400 flex items-center cursor-pointer justify-center rounded-sm">
                                                    <i className="mdi mdi-trash-can text-white text-[14px]"></i>
                                                </div>
                                            </div>

                                        )
                                    })}

                                </div>
                            </div>

                            <br />
                            <div className="p-3 md:w-1/2 w-[300px] rounded-md">
                                <span className="flex justify-center items-center bg-white text-[12px] mb-1 text-red-500">{imageMessage}</span>
                                <div className="h-32 w-full overflow-hidden relative shadow-md border-2 items-center rounded-md cursor-pointer   border-gray-400 border-dotted">
                                    <input type="file" onChange={handleImage} className="h-full w-full opacity-0 z-10 absolute" name="files[]" />
                                    <div className="h-full w-full bg-gray-200 absolute z-1 flex justify-center items-center top-0">
                                        <div className="flex flex-col">
                                            <i className="mdi mdi-folder-open text-[30px] text-gray-400 text-center"></i>
                                            <span className="text-[12px] text-black">{`Drag and Drop images for your mod`}</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex flex-wrap gap-2 mt-2">
                                    {images.map((file, key) => {
                                        return (

                                            <div key={key} className='w-full h-16 flex items-center justify-between rounded p-3 bg-white'>
                                                <div className="flex flex-row items-center gap-2">
                                                    <div className="h-12 w-12 ">
                                                        <div className="w-full h-full rounded">
                                                            <img className="w-full h-full rounded" src={URL.createObjectURL(file)} />
                                                        </div>

                                                    </div>
                                                    <span className="truncate w-44 text-black">{file.name}</span>
                                                </div>
                                                <div onClick={() => { removeImage(file.name) }} className="h-6 w-6 bg-red-400 flex items-center cursor-pointer justify-center rounded-sm">
                                                    <i className="mdi mdi-trash-can text-white text-[14px]"></i>
                                                </div>
                                            </div>

                                        )
                                    })}
                                    {addedImages.map((file, key) => {
                                        return (

                                            <div key={key} className='w-full h-16 flex items-center justify-between rounded p-3 bg-white'>
                                                <div className="flex flex-row items-center gap-2">
                                                    <div className="h-12 w-12 ">
                                                        <div className="w-full h-full rounded">
                                                            <img className="w-full h-full rounded" src={URL.createObjectURL(file)} />
                                                        </div>

                                                    </div>
                                                    <span className="truncate w-44 text-black">{file.name}</span>
                                                </div>
                                                <div onClick={() => { removeImage(file.name) }} className="h-6 w-6 bg-red-400 flex items-center cursor-pointer justify-center rounded-sm">
                                                    <i className="mdi mdi-trash-can text-white text-[14px]"></i>
                                                </div>
                                            </div>

                                        )
                                    })}
                                </div>
                            </div>

                            <br />
                            <br />
                            <div>
                                <TagsInput
                                    value={tags}
                                    onChange={(e) => setTags(e)}
                                    name="tags"
                                    placeHolder="enter tags that describe the mod"
                                    classNames={{ tag: 'text-black', input: 'text-black' }}
                                />
                                <em>press enter to add new tag</em>
                            </div>
                            <br />
                            <button type="submit" id="" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Save Mod</button>
                        </div>




                    </form>
                </div>


            </>
            );
        }
        return (<div>no mod found</div>);
    }
    return (<div>no mod found</div>);
}

export async function getServerSideProps() {
    return { props: {} };
}

async function getDataAsync(id: string) {
    return await trpc.mods.getByIdWithFiles.useQuery(id).data
}