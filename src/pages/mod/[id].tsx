import { trpc } from "../../utils/trpc";
import { useRouter } from "next/router";
import type { ModC } from "../../types/Mod";
import type { ImageC } from "../../types/Image";
import ModDetails from "../components/Mod/ModDetails";
import mods from "../api/mods";

export default function Mod() {
    const router = useRouter()
    const id = router.query.id

    if (typeof id === "string") {
        const { data: mod, refetch: reload } = trpc.mods.getById.useQuery(id);
        
        if (mod) {  
            const n: ModC = {
                id: mod.id,
                title: mod.title,
                modid: mod.modid,
                description: mod.description,
                downloads: mod.downloads,
                follows: mod.Follows.length,
                comments: mod.Comments,
                Assets: mod.ModHasAssets.map(asset => asset.Asset),
                releases: mod.Releases,
                author: mod.User.name,
                createdAt: mod.createdAt,
                updatedAt: mod.updatedAt,
                images: mod.Images,
                tags: mod.ModHasTags.map(t => t.Tag.name)
            }

            return (<ModDetails mod={n} onContentChanged={reload}></ModDetails>);
        }
        return (<div>no mod found</div>);
    }
    return (<div>no mod found</div>);
}

export async function getServerSideProps() {
    return { props: {} };
}