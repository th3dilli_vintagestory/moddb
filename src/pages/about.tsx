import type { Person } from "../types/About/Person";
import PersonListItem from "./components/About/PersonListItem";

function About() {
    const persons: Person[] = [ 
        { id: 0, firstName: 'Stanislav', lastName: 'Bernatskyi', gitlabTag: 'stasbern1', role: 'Developer' }, 
        { id: 1, firstName: 'Manuel', lastName: 'Dielacher', gitlabTag: 'Th3Dilli', role: 'Developer'  },
        { id: 2, firstName: 'Felix', lastName: 'Gaggl', gitlabTag: 'felixgaggl', role: 'Developer'  },
        { id: 3, firstName: 'Felix', lastName: 'Mitterer', gitlabTag: 'fischly', role: 'Developer'  }
    ];
    
    return (<>
        <div className="container mx-auto">
            <h1 className="text-4xl mt-4 text-center">Idea</h1>
            <div className="mt-4 bg-slate-800 p-2 m-1 rounded-xl text-center shadow-inner shadow-slate-900">
                <p className="text-lg m-3">
                    VSModDB is a website that enables easy handling and presenting of mods (<i>game modifications</i>) for the game Vintagestory, a sandbox survival game similar to Minecraft.
                </p>
            </div>

            <h1 className="mt-7 text-4xl mt-4 text-center">Team</h1>
            <div className="grid grid-cols-2 gap-4 my-6">
                {persons.map(person => (
                        <PersonListItem key={person.id} person={person}></PersonListItem>
                ))}
            </div>
        </div>
        </>)

}

export default About;