import { type NextPage } from "next";
import { trpc } from "../utils/trpc";

const Home: NextPage = () => {
  const hello = trpc.mods.getAll.useQuery();

  return (
    <>
      <div>hello world</div>
    </>
  );
};

export default Home;
