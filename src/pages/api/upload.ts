import type { NextApiRequest, NextApiResponse } from 'next';
import formidable from 'formidable';
import { getServerAuthSession } from "../../server/common/get-server-auth-session";
import { env } from '../../env/server.mjs';
import { mkdirSync, existsSync, unlink, readFileSync } from 'fs';
import JSZip from 'jszip';
import type { ModInfo } from '../../types/ModInfo';

export const config = {
    api: {
        bodyParser: false,
    },
};

const upload = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getServerAuthSession({ req, res });
    if (session) {
        let filepath: string;
        const path = env.UPLOAD_TMP_DIR + session.user?.id
        if (!existsSync(path))
            mkdirSync(path, { recursive: true });

        const form = new formidable.IncomingForm({
            uploadDir: path,
            maxFileSize: Number(env.UPLOAD_MAX_FILESIZE) * 1024 * 1024,
        });

        form.on("fileBegin", (_, file) => {
            filepath = file.filepath;
        });
        
        form.parse(req, (err, fields, files) => {
            console.log('FORM PARSE: ', err, fields, files);
            if (err) {
                if (filepath) {
                    unlink(filepath, (err) => console.log(err));
                }
                res.send(err);
            }
            const file = files["file"];
            if (file && !Array.isArray(file) && file.originalFilename?.includes(".zip")) {
                const zip = new JSZip();
                zip.loadAsync(readFileSync(filepath)).then(data => {
                    data.file("modinfo.json")?.async("string").then(data => {
                        const modinfo: ModInfo = JSON.parse(data)
                        console.log(modinfo)
                        // send temp file path
                        // also send back modinfo if a mod was uploaded
                        res.send({ files, modinfo })
                    });
                })
            } else {
                // send temp file path
                // so it can be moved to a final path later and referenced in the database
                res.send(files)
            }
        });
    } else { res.json({ error: 'This is a protected route', code: 401 }); }
};

export default upload;
