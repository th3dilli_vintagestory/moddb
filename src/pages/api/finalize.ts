import type { NextApiRequest, NextApiResponse } from 'next';
import { getServerAuthSession } from "../../server/common/get-server-auth-session";
import { env } from '../../env/server.mjs';
import { mkdirSync, existsSync, renameSync } from 'fs';
import type { FileC } from '../../types/File';

export const config = {
    api: {
        bodyParser: true,
    },
};

interface ExtendedNextApiRequest extends NextApiRequest {
    body: FileC[]
}

const finalize = async (req: ExtendedNextApiRequest, res: NextApiResponse) => {
    const session = await getServerAuthSession({ req, res });

    if (session) {
        const baseTempPath = env.UPLOAD_TMP_DIR + session.user?.id
        const baseFinalPath = env.UPLOAD_DIR + session.user?.id;

        const files = req.body;
        console.log('FILES: ', files);
        try {
            if (!existsSync(baseFinalPath)) {
                mkdirSync(baseFinalPath, { recursive: true });
            }

            for (const file of files) {
                console.log('FILE: ', file);
                if (file.newFilename.includes('..')) continue;

                // TODO: use path.join() for path joinen
                const tempFile = baseTempPath + '/' + file.newFilename;
                const finalFile = baseFinalPath + '/' + file.newFilename;
                file.publicUri = env.UPLOAD_DIR_URI + session.user?.id + '/' + file.newFilename;
                console.log('MOVING "', tempFile, '" to "', finalFile, '" public uri: "', file.publicUri, '"');

                renameSync(tempFile, finalFile);
            }

            res.json(files);
        } catch(ex) {
            console.error('Error while finalizing files: ', ex);
            res.json({ error: '', code: 500 });
        }
    } else { res.json({ error: 'This is a protected route', code: 401 }); }
};

export default finalize;
