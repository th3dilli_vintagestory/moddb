import { type NextApiRequest, type NextApiResponse } from "next";

import { prisma } from "../../server/db/client";

const mod = async (req: NextApiRequest, res: NextApiResponse) => {
    const { id  } = req.query
    const ids = id as string;
    if(ids){
        const mod = await prisma.mod.findFirst({where:{id: ids}});
        res.status(200).json(mod);
    }
};

export default mod;
