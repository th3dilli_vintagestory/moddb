import { useState } from "react";
import { trpc } from "../utils/trpc";
import { useRouter } from "next/router";
import { TagsInput } from "react-tag-input-component";
import logo from '../../public/images/logo.png'
import Image from 'next/image'

export default function CreateMod() {

  const trpcQuery = trpc.mods.createMod.useMutation();
  const router = useRouter();

  const [message, setMessage] = useState<string>();
  const [files, setFile] = useState<File[]>([]);
  const [imageMessage, setImageMessage] = useState<string>();
  const [images, setImages] = useState<File[]>([]);

  const [imagesData, setImagesData] = useState([{
    data: "",
    name: "",
    contentType: ""
  }]);

  const [state, setState] = useState({
    title: "",
    description: "",
    mod_filename: ""
  });

  const [tags, setTags] = useState<string[]>([]);

  function handleChange(e: { target: { files: any; name: any; value: any; }; }) {
    if (e.target.files) {
      setState({ ...state, mod_filename: e.target.files[0].name });

    } else {
      setState({ ...state, [e.target.name]: e.target.value });
    }
  }

  const handleFile = (e: { target: { files: any; }; }) => {
    setMessage("");
    const file = e.target.files;
    for (let i = 0; i < file.length; i++) {
      const fileType = file[i]['type'];
      const validImageTypes = ['application/x-zip-compressed', 'application/zip', 'application/zip-compressed'];
      if (validImageTypes.includes(fileType)) {
        setFile([...files, file[i]]);
      } else {
        setMessage("Please upload a ZIP File");
      }
    }
  };

  const handleImage = (e: { target: { files: any; }; }) => {
    setImageMessage("");
    const file = e.target.files;
    for (let i = 0; i < file.length; i++) {
      const fileType = file[i]['type'];
      const validImageTypes = ['image/png', 'image/jpeg'];
      if (validImageTypes.includes(fileType)) {
        setImages([...images, file[i]]);
        const reader = new FileReader();
        reader.readAsDataURL(file[i]);
        reader.onload = async function () {
          if (typeof (reader.result) === "string") {
            setImagesData([...imagesData, { data: reader.result, name: file[i].name, contentType: fileType }]);
          }

        }
      } else {
        setImageMessage("Please upload an Image");
      }
    }
  };


  async function handleSubmit(e: { preventDefault: () => void; }) {
    e.preventDefault();

    // convert data file upload into base64 format and send to server, TODO include all necessary fields
    const reader = new FileReader();
    if (files[0] && imagesData[0]) {
      reader.readAsDataURL(files[0]);
      reader.onload = async function () {
        await trpcQuery.mutateAsync({ title: state.title, description: state.description, modFile: reader.result, mod_filename: files[0]!.name, tags: tags, images: imagesData });
        router.push(`/mods/`);
      };
      reader.onerror = function (error) {
        console.log(error);
      };
    } else if (!files[0] && imagesData[0]) {
      await trpcQuery.mutateAsync({ title: state.title, description: state.description, tags: tags, mod_filename: "", images: imagesData });
      router.push(`/mods/`);
    } else if (files[0] && !imagesData[0]) {
      reader.readAsDataURL(files[0]);
      reader.onload = async function () {
        await trpcQuery.mutateAsync({ title: state.title, description: state.description, modFile: reader.result, mod_filename: files[0]!.name, tags: tags, images: [] });
        router.push(`/mods/`);
      };
      reader.onerror = function (error) {
        console.log(error);
      };
    } else {
      await trpcQuery.mutateAsync({
        title: state.title, description: state.description, tags: tags, mod_filename: "",
        images: []
      });
      router.push(`/mods/`);
    }



  }

  const removeFile = (i: string) => {
    setFile(files.filter(x => x.name !== i));
  }

  const removeImage = (i: string) => {
    setImages(images.filter(x => x.name !== i));
    setImagesData(imagesData.filter(x => x.name !== i));
  }

  return (
    <>
      <div className="card bg-card silver">


        <form onSubmit={handleSubmit} className="flex flex-col justify-between form-control" autoComplete="off">
          <div >
            <input type="text" name="title" value={state.title} onChange={handleChange} placeholder="Title" id="titleInput" className="bg-black h-10 rounded-md outline-0 pl-4 w-96" required />
            <br />
            <br />
            <input type="text" name="description" value={state.description} onChange={handleChange} placeholder="Description" id="descriptionInput" className="bg-black h-10 rounded-md outline-0 pl-4 w-96" required />
            <br />
            <br />


            <div className="p-3 md:w-1/2 w-[300px] rounded-md">
              <span className="flex justify-center items-center bg-white text-[12px] mb-1 text-red-500">{message}</span>
              <div className="h-32 w-full overflow-hidden relative shadow-md border-2 items-center rounded-md cursor-pointer   border-gray-400 border-dotted">
                <input type="file" onChange={handleFile} className="h-full w-full opacity-0 z-10 absolute" name="files[]" />
                <div className="h-full w-full bg-gray-200 absolute z-1 flex justify-center items-center top-0">
                  <div className="flex flex-col">
                    <i className="mdi mdi-folder-open text-[30px] text-gray-400 text-center"></i>
                    <span className="text-[12px] text-black">{`Drag and Drop the mod file`}</span>
                  </div>
                </div>
              </div>
              <div className="flex flex-wrap gap-2 mt-2">
                {files.map((file, key) => {
                  return (

                    <div key={key} className='w-full h-16 flex items-center justify-between rounded p-3 bg-white'>
                      <div className="flex flex-row items-center gap-2">
                        <div className="h-12 w-12 ">
                          <div className="w-full h-full rounded">
                            <Image src={logo} alt={""} />
                          </div>

                        </div>
                        <span className="truncate w-44 text-black">{file.name}</span>
                      </div>
                      <div onClick={() => { removeFile(file.name) }} className="h-6 w-6 bg-red-400 flex items-center cursor-pointer justify-center rounded-sm">
                        <i className="mdi mdi-trash-can text-white text-[14px]"></i>
                      </div>
                    </div>

                  )
                })}
              </div>
            </div>

            <br />
            <div className="p-3 md:w-1/2 w-[300px] rounded-md">
              <span className="flex justify-center items-center bg-white text-[12px] mb-1 text-red-500">{imageMessage}</span>
              <div className="h-32 w-full overflow-hidden relative shadow-md border-2 items-center rounded-md cursor-pointer   border-gray-400 border-dotted">
                <input type="file" onChange={handleImage} className="h-full w-full opacity-0 z-10 absolute" name="files[]" />
                <div className="h-full w-full bg-gray-200 absolute z-1 flex justify-center items-center top-0">
                  <div className="flex flex-col">
                    <i className="mdi mdi-folder-open text-[30px] text-gray-400 text-center"></i>
                    <span className="text-[12px] text-black">{`Drag and Drop images for your mod`}</span>
                  </div>
                </div>
              </div>
              <div className="flex flex-wrap gap-2 mt-2">
                {images.map((file, key) => {
                  return (

                    <div key={key} className='w-full h-16 flex items-center justify-between rounded p-3 bg-white'>
                      <div className="flex flex-row items-center gap-2">
                        <div className="h-12 w-12 ">
                          <div className="w-full h-full rounded">
                            <img className="w-full h-full rounded" src={URL.createObjectURL(file)} />
                          </div>

                        </div>
                        <span className="truncate w-44 text-black">{file.name}</span>
                      </div>
                      <div onClick={() => { removeImage(file.name) }} className="h-6 w-6 bg-red-400 flex items-center cursor-pointer justify-center rounded-sm">
                        <i className="mdi mdi-trash-can text-white text-[14px]"></i>
                      </div>
                    </div>

                  )
                })}
              </div>
            </div>

            <br />
            <br />
            <div>
              <TagsInput
                value={tags}
                onChange={setTags}
                name="tags"
                placeHolder="enter tags that describe the mod"
                classNames={{ tag: 'text-black', input: 'text-black' }}
              />
              <em>press enter to add new tag</em>
            </div>
            <br />
            <button type="submit" id="" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Save Mod</button>
          </div>




        </form>
      </div>
    </>
  )

}



export async function getServerSideProps() {
  return { props: {} };
}
