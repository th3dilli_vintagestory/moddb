import { ChangeEventHandler, DetailedHTMLProps, FormEventHandler, SyntheticEvent, useState, useEffect } from "react";
import { ModC } from "../types/Mod";
import { trpc } from "../utils/trpc";
import ModFilters from "./components/ModFilters"
import ModCard from "./components/Mod/ModCard";
import React from "react";
import { sortModsByDownloads, sortModsByFollows, sortModsByName } from '../utils/utils';

const options = [
    { value: 'name', display: 'Name' },
    { value: 'downloads', display: 'Downloads' },
    { value: 'follows', display: 'Follows' },
];

export default function Mods() {
    const [search, setSearch] = useState("");
    const [sortby, setSortby] = useState('name');
    const [authorValue, setAuthorValue] = useState('default');
    const [tagValue, setTagValue] = useState('default');

    const { data: modb } = trpc.mods.getAll.useQuery();

    const modss = modb?.map((mod: any, idx: any) => {
        const n: ModC = {
            ...mod,
            id: mod.id,
            title: mod.title,
            modid: mod.modid,
            description: mod.description,
            downloads: mod.downloads || 0,
            follows: mod.Follows.length,
            images: mod.Images,
            author: mod.User.name,
            tags: mod.ModHasTags.map((t: any) => t.Tag.name)
        }
        return n;
    });

    const [mods, setMods] = useState<ModC[] | undefined>(modss);
    useEffect(() => {
        setMods(sortModsByName(modss));
    }, [modb]);

    function getFiltersNames(mods: ModC[] | undefined) {
        let authors: string[] = []
        let tags: string[] = []

        mods?.forEach((mod) => {
            authors = [...authors, mod.author]
            tags = [...tags, ...mod.tags]
          })
      
          return {
            authors: Array.from(new Set(authors)),
            tags: Array.from(new Set(tags)),
          }
    }

    function filterData(data: ModC[] | undefined) {
        let filteredData: ModC[] | undefined = [];
        filteredData = filterBySearch(data)
        filteredData = filterByAuthor(filteredData)
        filteredData = filterByTag(filteredData)
        filteredData = filterBySort(filteredData)

        setMods(filteredData)
    }

    function filterByAuthor(data: ModC[] | undefined): ModC[] | undefined {
        if (authorValue === "default") return data
        return data?.filter(mod => mod.author === authorValue)
    }

    function filterByTag(data: ModC[] | undefined): ModC[] | undefined {
        if (tagValue === "default") return data
        return data?.filter(mod => mod.tags.includes(tagValue))
    }

    function filterBySearch(data: ModC[] | undefined): ModC[] | undefined {
        return data?.filter(m => m.title.toLowerCase().replace(/\s/g, "").includes(search.toLowerCase().replace(/\s/g, "")) || m.description.toLowerCase().replace(/\s/g, "").includes(search.toLowerCase().replace(/\s/g, ""))).map(mod => {
            const n: ModC = {
                ...mod,
                id: mod.id,
                title: mod.title,
                modid: mod.modid,
                description: mod.description,
                downloads: mod.downloads || 0
            }
            return n;
        });
    }

    function filterBySort(data: ModC[] | undefined): ModC[] | undefined {
        let sortedMods: ModC[] | undefined = []
        if (sortby === 'name') {
            sortedMods = sortModsByName(data, false);
        }
        if (sortby === 'downloads') {
            sortedMods = sortModsByDownloads(data, false);
        }
        if (sortby === 'follows') {
            sortedMods = sortModsByFollows(data, false);
        }
        return sortedMods
    }

    useEffect(() => {
        filterData(modss)
    }, [search, sortby, authorValue, tagValue])

    const { authors, tags } = getFiltersNames(modss);

    return (
        <div className="p-3">
            <div className="flex align-center bp-5 mb-8 justify-center mb-6">
                <div>
                    {/* Sort */}
                    <div className="">
                        <label
                            htmlFor='sorting'
                            className='block text-sm font-sm text-gray-500 italic w-full'
                        >
                            Sort By
                        </label>
                        <select
                            id='sorting'
                            name='sorting'
                            onChange={handleSort}
                            className='mt-1 block text-black pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-sm'
                            defaultValue={sortby}
                        >
                            {options.map((option) => (
                                <option key={option.value} value={option.value}>
                                    {option.display}
                                </option>
                            ))}
                        </select>
                    </div>
                </div>
                <ModFilters onAuthorChange={(e: any) => setAuthorValue(e.target.value)} onTagChange={(e: any) => setTagValue(e.target.value)} authors={authors} tags={tags}/>
                <form className="ml-10" onSubmit={handleSubmit}>
                    <label htmlFor="search" className='block text-sm font-sm text-gray-500 italic w-full'>
                        Search
                        </label>
                    <input id="search" 
                    className='mt-1 block text-black pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-sm'
                    type="text" value={search} onChange={handleChange} />
                </form>
            </div>
            {!mods || mods.length === 0 ? <span>Nothing Found</span> : <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 2xl:grid-cols-7 gap-5">
                {mods && mods.map((mod: any, index: any) => (<ModCard key={index} mod={mod}></ModCard>))}
            </div>}
        </div>);

    function handleChange(event: any) {
        setSearch(event.target.value);
    }

    function handleSort(e: any) {
        e.preventDefault();
        const newSortby = e.target.value;
        setSortby(newSortby);
    }

    function handleSubmit(event: any) {
        console.log(event);
        event.preventDefault();
    }
}

export async function getServerSideProps() {
    return { props: {} };
  }