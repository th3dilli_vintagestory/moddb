export interface ReleaseC {
    id: string
    modid: string
    fileid: string
    version: string
    changelog: string
    hidden: boolean | null
    downloads: number | null
    createdAt: Date | null
    updatedAt: Date | null
}