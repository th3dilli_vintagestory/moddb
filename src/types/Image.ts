export interface ImageC {
    id: string
    modid: string
    url: string
    contentType: string | null
    fileName: string | null
}