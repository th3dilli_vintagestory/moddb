export interface FileC {
    originalFilename: string
    newFilename: string
    filepath: string
    mtime: string
    mimetype: string
    size: number
    dataURL?: string
    publicUri?: string
}