import type { CommentC } from "./Comment"
import type { ImageC } from "./Image"
import { ReleaseC } from "./Release"
import { Asset } from "@prisma/client"

export interface ModC {
    id: string
    modid: string
    title: string
    description: string
    downloads: number
    follows: number
    comments?: CommentC[]
    releases?: ReleaseC[]
    Assets?: Asset[]
    author: string
    tags: string[]
    createdAt: Date | null
    updatedAt: Date | null
    images: ImageC[]
}
