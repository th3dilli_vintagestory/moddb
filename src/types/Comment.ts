import type { UserC } from "./User"

export interface CommentC {
    id: string
    modid: string,
    userid: string,
    Author: UserC,
    content: string,
    postedAt: Date | null,
    replytoid: string | null
}