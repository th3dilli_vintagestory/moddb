

export interface UserC {
    id: string
    name: string
    email: string
    image: string | null
}