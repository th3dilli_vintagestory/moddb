export interface Person {
    id: number,
    firstName: string,
    lastName: string,
    gitlabTag: string,
    role: string | null
}