import { randomUUID } from "crypto";
import path from "path";
import { z } from "zod";
import { router, publicProcedure, protectedProcedure } from "../trpc";
import { writeFile, readFileSync, mkdirSync } from "fs";
import { env } from '../../../env/server.mjs';

const jsonDirectory = path.join(process.cwd(), 'uploads/final/');
const imageDirectory = path.join(process.cwd(), 'public/images/modImages/');

// create object for handling upload data
const createMod = z.object({
  title: z.string(),
  description: z.string(),
  modFile: z.any(),
  mod_filename: z.string(),
  tags: z.array(z.string()),
  images: z.array(z.any()),
})

// create object for handling upload data
const updateMod = z.object({
  title: z.string(),
  description: z.string(),
  modFile: z.any(),
  mod_filename: z.string(),
  tags: z.array(z.string()),
  id: z.string(),
  images: z.array(z.any()),
})

export const modsRouter = router({
  getAll: publicProcedure.query(({ ctx }) => {
    return ctx.prisma.mod.findMany({ include: { User: true, Follows: true, Images: true, ModHasTags: { include: { Tag: true } } } });
  }),
  getById: publicProcedure.input(z.string()).query(({ ctx, input }) => {
    return ctx.prisma.mod.findFirst({ where: { id: input }, include: { User: true, Follows: true, Images: true, Releases: true, Comments: { include: { Author: true } }, ModHasAssets: {include : {Asset: true}}, ModHasTags: { include: { Tag: true } } } });
  }),
  getByIdWithFiles: publicProcedure.input(z.string()).query(async ({ ctx, input }) => {
    const mod = await ctx.prisma.mod.findFirst({ where: { id: input }, include: { User: true, Follows: true, Images: true, ModHasAssets: {include : {Asset: true}}, ModHasTags: { include: { Tag: true } } } });
    if(mod){
      if(mod.ModHasAssets.length! > 0 && mod.Images.length! > 0){
        const data = await readFileSync(jsonDirectory + mod?.ModHasAssets[0]?.Asset.filelocation, 'base64');
        const imageArray = [];
        for (let index = 0; index < mod.Images.length; index++) {
          const element = mod.Images[index];
          if(element!.url !== ""){
            const imageData = await readFileSync(env.UPLOAD_DIR + mod.id + '/' + element!.fileName, 'base64');
            imageArray.push(imageData);  
          }
        }
        return { mod: mod, data: data, imageArray: imageArray };
      }else if(mod.ModHasAssets.length! > 0 && mod.Images.length! == 0){
        const data = <string> await readFileSync(jsonDirectory + mod?.ModHasAssets[0]?.Asset.filelocation, 'base64');
        return { mod: mod, data: data, imageArray: []};
      }else if(mod.ModHasAssets.length! == 0 && mod.Images.length! > 0){
        const imageArray = [];
        for (let index = 0; index < mod.Images.length; index++) {
          const element = mod.Images[index];
          if(element!.url !== ""){
            const imageData = await readFileSync(env.UPLOAD_DIR + mod.id + '/' + element!.fileName, 'base64');
            imageArray.push(imageData);  
          }
        }
        return { mod: mod, data: [], imageArray: imageArray };
      }else{
        return { mod: mod, data: [], imageArray: [] };
      }
      

    }else{
      return;
    }
  }),

  getTags: publicProcedure.input(z.array(z.number())).query(({ ctx, input }) => {
    return ctx.prisma.tag.findMany({ where: { id: { in: input } } })
  }),

  getFollowOfCurrentUser: protectedProcedure.input(z.string()).query(({ ctx, input }) => {
    return ctx.prisma.follow.findFirst({
      where: {
        userid: ctx.session.user.id,
        modid: input
      }
    });
  }),

  addFollow: protectedProcedure.input(z.string()).mutation(({ ctx, input }) => {
    return ctx.prisma.follow.create({
      data:  {
        userid: ctx.session.user.id,
        modid: input
      }
    });
  }),

  removeFollow: protectedProcedure.input(z.string()).mutation(({ ctx, input }) => {
    return ctx.prisma.follow.delete({
      where:  {
        userid_modid: {
          userid: ctx.session.user.id,
          modid: input
        }
      }
    });
  }), 

  createMod: publicProcedure.input(createMod).mutation(async ({ ctx, input }) => {
    const user = await ctx.prisma.user.findFirst();
    const userID = ctx.session?.user?.id || user!.id;
    const mod = await ctx.prisma.mod.create({ data: { id: randomUUID(), modid: `mod_modid`, title: input.title, description: input.description, downloads: 0, ownerid: userID, side: "both", sourceUrl: ``, draft: false } })

    if (input.mod_filename !== "") {
      writeFile(jsonDirectory + input.mod_filename, input.modFile.split(',')[1], 'base64', function (err: any) {
        console.log(err);
      });
      const asset = await ctx.prisma.asset.create({ data: { id: randomUUID(), filelocation: "" + input.mod_filename, url: env.UPLOAD_DIR_URI + input.mod_filename } });
      const modHasAsset = await ctx.prisma.modHasAsset.create({ data: { modid: mod.id, assetid: asset.id } });
    }

    if (input.images) {
      for (let index = 0; index < input.images.length; index++) {
        const element = input.images[index];
        if (element.data) {
          const dir = env.UPLOAD_DIR + mod.id;
          mkdirSync(dir, { recursive: true });
          writeFile(dir + '/' + element.name, element.data.split(',')[1], 'base64', function (err: any) {
              console.log(err);
          });
          const image = await ctx.prisma.image.create({ data: { id: randomUUID(),fileName: element.name, url: env.UPLOAD_DIR + mod.id + '/' + element.name, modid: mod.id, contentType: element.contentType } });

        }
        
        //TODO assign image to mod (?)

      }
    }


    for (let index = 0; index < input.tags.length; index++) {
      const tagExisting = await ctx.prisma.tag.findFirst({ where: { name: input.tags[index]! } });
      if (tagExisting) {
        const newTag = tagExisting;
        await ctx.prisma.modHasTag.create({ data: { modid: mod.id, tagid: newTag.id } });
      } else {
        const newTag = await ctx.prisma.tag.create({ data: { name: input.tags[index]! } });
        await ctx.prisma.modHasTag.create({ data: { modid: mod.id, tagid: newTag.id } });
      }



    }
    return
  }),
  deleteMod: publicProcedure.input(z.string()).mutation(async ({ ctx, input }) => {
    const user = await ctx.prisma.user.findFirst();
    const userID = ctx.session?.user?.id || user!.id;
    const mod = await ctx.prisma.mod.delete({where: {id: input}});

    
    return
  }),
  updateMod: publicProcedure.input(updateMod).mutation(async ({ ctx, input }) => {
    const user = await ctx.prisma.user.findFirst();
    const userID = user?.id || "";
    
    // Updating mod in database
    const mod = await ctx.prisma.mod.update({ where: { id: input.id }, data: { title: input.title, description: input.description } })
    const modExtended = await ctx.prisma.mod.findFirst({ where: { id: input.id }, include: { User: true, Follows: true, Images: true, ModHasAssets: {include : {Asset: true}}, ModHasTags: { include: { Tag: true } } } });
    // Update modFile for the entry
    // TODO delete old file relation
    if(modExtended && modExtended.ModHasAssets && modExtended.ModHasAssets.length > 0 && modExtended.ModHasAssets[0]){
      const deleteAsset = await ctx.prisma.asset.deleteMany({where: {id: modExtended.ModHasAssets[0]!.Asset.id}});
    }
    if (input.mod_filename !== "") {
      writeFile(jsonDirectory + input.mod_filename, input.modFile.split(',')[1], 'base64', function (err: any) {
        console.log(err);
      });
      const asset = await ctx.prisma.asset.create({ data: { id: randomUUID(), filelocation: "" + input.mod_filename, url: env.UPLOAD_DIR_URI + input.mod_filename } });
      const modHasAsset = await ctx.prisma.modHasAsset.create({ data: { modid: mod.id, assetid: asset.id } });
    }

    // Update images for the entry
    // TODO delete old images relation
    const deleteImages = await ctx.prisma.image.deleteMany({where: {modid: input.id}}); 
    if (input.images) {
      for (let index = 0; index < input.images.length; index++) {
        const element = input.images[index];
        if (element.data !== "") {
          const dir = env.UPLOAD_DIR + mod.id;
          mkdirSync(dir, { recursive: true });
          writeFile(dir + '/' + element.name, element.data.split(',')[1], 'base64', function (err: any) {
            console.log(err);
          });
          const image = await ctx.prisma.image.create({ data: { id: randomUUID(),fileName: element.name, url: env.UPLOAD_DIR_URI + mod.id + '/' + element.name, modid: mod.id } });

        }
      }
    }

    // Remove all tag relations and create new ones
    const modTagRelation = await ctx.prisma.modHasTag.deleteMany({ where: { modid: mod.id } });
    for (let index = 0; index < input.tags.length; index++) {
      const tagExisting = await ctx.prisma.tag.findFirst({ where: { name: input.tags[index]! } });
      if (tagExisting) {
        const newTag = tagExisting;
        await ctx.prisma.modHasTag.create({ data: { modid: mod.id, tagid: newTag.id } });
      } else {
        const newTag = await ctx.prisma.tag.create({ data: { name: input.tags[index]! } });
        await ctx.prisma.modHasTag.create({ data: { modid: mod.id, tagid: newTag.id } });
      }

    }
    return
  })
});
    
