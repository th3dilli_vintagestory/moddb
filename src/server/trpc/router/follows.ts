import { z } from "zod";
import { randomUUID } from 'crypto';

import { router, publicProcedure, protectedProcedure } from "../trpc";

export const followsRouter = router({
  getAllOfCurrentUser: protectedProcedure.query(({ ctx }) => {
    return ctx.prisma.follow.findMany({ where: { userid: ctx.session.user.id }, include: { Mod: { include: { Images: true }} } });
  })
});

