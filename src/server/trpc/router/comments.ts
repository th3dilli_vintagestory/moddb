import { z } from "zod";
import { randomUUID } from 'crypto';

import { router, protectedProcedure } from "../trpc";

export const commentsRouter = router({
  addComment: protectedProcedure.input(
    z.object({ 
      modid: z.string(),
      content: z.string(),
      replyTo: z.optional(z.string())
    }))
    .mutation(({ctx, input}) => {

      return ctx.prisma.comment.create({
        data: {
          id: randomUUID(),
          userid: ctx.session.user.id,
          modid: input.modid,
          content: input.content,
          replytoid: input.replyTo,
          postedAt: new Date()
        }
      });
    })
});

