import { router } from "../trpc";
import { authRouter } from "./auth";
import { commentsRouter } from "./comments";
import { followsRouter } from "./follows";
import { modsRouter } from "./mods";

export const appRouter = router({
  mods: modsRouter,
  comments: commentsRouter,
  follows: followsRouter,
  auth: authRouter
});

// export type definition of API
export type AppRouter = typeof appRouter;
