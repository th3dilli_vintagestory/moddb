const { parse }  = require('node-html-parser');

const https = require('https');
const fs = require('fs');

// helper to implement delay between requests, in order to not flood external APIs
const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

/**
 * Helper function that scrapes a mod's page and extracts the screenshot files.
 * This is needed, since the official API does not return screenshots, currently.
 * @param urlalias the mod's urlalias field
 * @returns list of screenshot links, in the form of /files/asset/<modid>/<filename>
 */
async function scrapeScreenshots(assetid) {
    const htmlScrape = await fetch(`https://mods.vintagestory.at/show/mod/${assetid}`);
    const html = await htmlScrape.text();

    // fs.writeFileSync(detailFile, html);
    const parsed = parse(html);
    const fotorama = parsed.querySelector('.imageslideshow.fotorama');
    const imageLinks = fotorama ? fotorama.querySelectorAll('img').map(i => i.getAttribute('src')) : [];

    return imageLinks;
}

/**
 * Helper function that downloads a given URL to a given local file.
 */
async function downloadFile(uri, filename, skipIfExists=true) {
    if (skipIfExists && fs.existsSync(filename)) {
        console.log(`[DownloadFile] Skipping '${filename}', since it already exists.`);
        return false;
    }

    fs.mkdirSync(filename.slice(0, filename.lastIndexOf('/')), { recursive: true });

    const file = fs.createWriteStream(filename);
    const request = https.get(uri, (res) => {
        res.pipe(file);

        file.on('finish', () => {
            file.close();
            console.log(`[DownloadFile] Finished downloading '${uri}'.`);
        });
    });

    return true;
}

/**
 * Downloads details about the top n most downloaded mods.
 * @param count How many mods should be downloaded.
 */
async function scrapeMods(count=10, skip=0) {
    const mods = require('./data/mods.json').mods.slice(skip, count);

    for (const mod of mods) {
        const detailUri = `https://mods.vintagestory.at/api/mod/${mod.modid}`;
        const detailFile = `./scraper/data/detail/${mod.modid}.json`;

        await downloadFile(detailUri, detailFile, true);
        await delay(4000);

        const screenshotLinks = await scrapeScreenshots(mod.assetid);
        console.log(screenshotLinks);

        // download comments
        const commentUrl = `https://mods.vintagestory.at/api/comments/${mod.assetid}`;
        const commentPath = `./scraper/data/comments/${mod.modid}.json`;

        await downloadFile(commentUrl, commentPath);
        await delay(2000);
    
        // download all screenshots
        const basefilePath = `./scraper/data/images`;

        for (let i = 0; i < screenshotLinks.length; i++) {
            const screenshotLink = screenshotLinks[i];
            const screenshotName = screenshotLink.slice(screenshotLink.lastIndexOf('/') + 1);
            
            const screenshotFile = `${basefilePath}/${mod.modid}/${screenshotName}`;
            await downloadFile(`https://mods.vintagestory.at${screenshotLink}`, screenshotFile);
            
            await delay(2000);
        }

        // download logo
        if (mod.logo) {
            const logoUri = `https://mods.vintagestory.at/${mod.logo}`;
            await downloadFile(logoUri, `${basefilePath}/${mod.modid}/_logo.png`);
            await delay(2000);
        }

    }
}

scrapeMods(30, 0);