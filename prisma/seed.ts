import { PrismaClient } from '@prisma/client'
import { randomInt, randomUUID } from 'crypto';
import { readdirSync, mkdirSync, copyFileSync, existsSync, readFileSync } from 'fs';
import { NodeHtmlMarkdown } from 'node-html-markdown';
import { parse } from 'node-html-parser';

const prisma = new PrismaClient()

async function seedWithDummyData() {
    const admin = await prisma.role.create({ data: { name: "Admin" } });
    const user = await prisma.role.create({ data: { name: "User" } });
    const moderator = await prisma.role.create({ data: { name: "Moderator" } });
    const time = new Date();
    const adminUser = await prisma.user.create({ data: { id: randomUUID(), email: "admin@users.at", name: "adminuser1", roleid: admin.id } });
    const moderatorUser = await prisma.user.create({ data: { id: randomUUID(), email: "moderator@users.at", name: "oderatoruser1", roleid: moderator.id } });
    const userUser = await prisma.user.create({ data: { id: randomUUID(), email: "user@users.at", name: "user1", roleid: user.id } });

    const craftingTag = await prisma.tag.create({data: {name: "Crafting"}});
    const creatureTag = await prisma.tag.create({data: {name: "Creature"}});

    const stoneQuarry = await prisma.mod.create({ data: { id: randomUUID(), modid: `stone_quarry`, title: 'Stone Quarry', downloads: 21339, ownerid: userUser.id, side: "both", sourceUrl: 'https://github.com/DArkHekRoMaNT/StoneQuarry', draft: false, createdAt: new Date('Nov 15 2021 4:57:00 AM GMT+0100'), updatedAt: new Date('Oct 7 2022 3:49 PM'),
        description: `Do you love making things out of stone, but hate the process of mining it? This mod is for you!\n\n- Create quarries to collect rocks as stone slabs.\n- Process stone slabs into rock, stones, bricks or polished rock.\n- Smash stones into gravel and sand inside rubble storage.\n- Store and transport stones, gravel and sand in rubble storage.\n- Produce muddy gravel in rubble storage! No more having go to take it from far away lakes!\nPractical guides can be found in the handbook\n\n||Known IMultiBlock-issues for 2.0.0||\n- Particles on the subblock are black/transparent (when you hit)\n- The breaking texture on the sub-block is missing\n- emitSideAO doesn't work on subblock\n- Selboxes are always hidden on the subblock||\n\n---\n\n*This is the new version (fork) of [Quarry Works!](https://mods.vintagestory.at/show/mod/44) for 1.15+*` } });
    ['/images/placeholder/0_quarry.png', '/images/placeholder/1_quarry.png', '/images/placeholder/2_quarry.png'].forEach(async img => {
        await prisma.image.create({ data: { id: randomUUID(), url: img, contentType: 'image/png', fileName: img.slice(img.lastIndexOf('/') + 1), modid: stoneQuarry!.id }});
    });
    await prisma.modHasTag.create({data: { modid: stoneQuarry.id, tagid: craftingTag.id}});
    await prisma.modHasTag.create({data: { modid: stoneQuarry.id, tagid: creatureTag.id}}); 

    const asset = await prisma.asset.create({ data: { id: randomUUID(), filelocation: '/upload/stone-quarry.zip', url: '/api/files/stone-quarry.zip' } });
    await prisma.release.create({ data: { id: randomUUID(), changelog: 'Finally 1.0.0!!11eleven', version: '1.0.0', downloads: 1337, modid: stoneQuarry.id, fileid: asset.id, createdAt: new Date('Nov 15 2021 4:57:00 AM GMT+0100') }});
    await prisma.release.create({ data: { id: randomUUID(), changelog: 'We changed EVEN MORE in this release.', version: '0.2.0', downloads: 133, modid: stoneQuarry.id, fileid: asset.id, createdAt: new Date('Okt 31 2021 8:53:00 AM GMT+0100') }});
    await prisma.release.create({ data: { id: randomUUID(), changelog: 'We changed A LOT in this release.', version: '0.1.1', downloads: 21, modid: stoneQuarry.id, fileid: asset.id, createdAt: new Date('Apr 08 2021 2:51:00 AM GMT+0100')  }});

    for (let index = 0; index < 20; index++) {
        const mod = await prisma.mod.create({ data: { id: randomUUID(), modid: `mod_${index}_modid`, title: `sampel mod ${index} title`, description: `This is an example mod ${index}. With it you can do example things.`, downloads: randomInt(100, 10000), ownerid: userUser.id, side: "both", sourceUrl: `https://gitlab.com/modder/myrepo${index}`, draft: false } })
        await prisma.modHasTag.create({data:{modid: mod.id, tagid: craftingTag.id}});
        await prisma.modHasTag.create({data:{modid: mod.id, tagid: creatureTag.id}});

        const image1 = await prisma.image.create({ data: { id: randomUUID(), url: '/images/placeholder/mod-image-1.jpeg', contentType: 'image/jpeg', fileName: 'mod-image-1.jpeg', modid: mod!.id }});
        const image2 = await prisma.image.create({ data: { id: randomUUID(), url: '/images/placeholder/mod-image-2.jpeg', contentType: 'image/jpeg', fileName: 'mod-image-2.jpeg', modid: mod!.id }});
        const image3 = await prisma.image.create({ data: { id: randomUUID(), url: '/images/placeholder/mod-image-3.jpeg', contentType: 'image/jpeg', fileName: 'mod-image-3.jpeg', modid: mod!.id }});
    }

    const modWithComment = await prisma.mod.findFirst({ where: { modid: 'mod_0_modid' } });
    const comment = await prisma.comment.create({ data: { id: randomUUID(), userid: userUser.id, modid: modWithComment!.id, content: `Hello world! This is a comment.` }});

    const commentReply = await prisma.comment.create({ data: { id: randomUUID(), userid: adminUser.id, modid: modWithComment!.id, content: 'This is a reply!', replytoid: comment.id }});
}

async function seedWithRealData() {
    // create roles
    const admin = await prisma.role.create({ data: { name: "Admin" } });
    const moderator = await prisma.role.create({ data: { name: "Moderator" } });
    const user = await prisma.role.create({ data: { name: "User" } });

    // load previously scraped modinfo
    const dataDir = './scraper/data'; // TODO: move to .env

    // load authors
    const authors = require(`../${dataDir}/authors.json`).authors;

    // load tags
    const tags = require(`../${dataDir}/tags.json`).tags;
    for (const tag of tags) {
        await prisma.tag.create({ data: { id: tag.tagid, name: tag.name } });
    }

    let modCounter = 0;
    const files = readdirSync(dataDir + '/detail');
    for (const file of files) {
        const modinfo = require(`../${dataDir}/detail/${file}`).mod;
        
        // remove <div class="spoiler"> elements and replace them with our custom spoiler markdown syntax: ||spoiler title||spoiler content||
        const parsedDescription = parse(modinfo.text);
        const spoilers = parsedDescription.querySelectorAll('div.spoiler');
        
        for (let spoiler of spoilers) {
            const toggle = spoiler.querySelector('div.spoiler-toggle');
            const content = spoiler.querySelector('div.spoiler-text');
            const replaceText = `\n<br/><br/>\n||${toggle?.innerText ?? 'Spoiler'}||\n${content?.innerHTML ?? ''}\n||\n<br/><br/>`;
            
            spoiler.replaceWith(replaceText);
        }
        
        // create author user
        const modAuthor = await prisma.user.upsert({ where: { email: `${modinfo.author}@vintagestory.at` }, update: {}, create: { id: randomUUID(), email: `${modinfo.author}@vintagestory.at`, name: modinfo.author, roleid: user.id } });
        
        // create mod
        const mod = await prisma.mod.create({ data: { id: randomUUID(), modid: modinfo.modid.toString(), title: modinfo.name, description: NodeHtmlMarkdown.translate(parsedDescription.innerHTML), downloads: modinfo.downloads, ownerid: modAuthor.id, side: modinfo.side, sourceUrl: modinfo.sourcecodeurl, createdAt: new Date(modinfo.created), updatedAt: new Date(modinfo.lastmodified), draft: false } });

        // add tags to mod
        for (const modtag of modinfo.tags) {
            await prisma.modHasTag.create({ data: { modid: mod.id, tagid: tags.find((t: any) => t.name == modtag).tagid } });
        }

        // add comments
        const commentPath = `${dataDir}/comments/${modinfo.modid}.json`;
        if (existsSync(commentPath)) {
            const comments = require(`../${commentPath}`).comments;

            const userCount = await prisma.user.count();
            for (const comment of comments) {
                if (!comment.text) continue; 

                const commentAuthorName = authors.find((a: any) => a.userid == comment.userid);
                if (!commentAuthorName) continue;

                const commentAuthor = await prisma.user.upsert({ where: { email: `${commentAuthorName.name}@vintagestory.at` }, update: {}, create: { id: randomUUID(), email: `${commentAuthorName.name}@vintagestory.at`, name: commentAuthorName.name, roleid: user.id } });

                const skip = Math.floor(Math.random() * userCount);
                await prisma.comment.create({ data: { id: randomUUID(), userid: commentAuthor.id, modid: mod.id, content: NodeHtmlMarkdown.translate(comment.text), postedAt: new Date(comment.created) }});
            }
        }

        // add images
        const imagePath = `${dataDir}/images/${modinfo.modid}`;
        if (existsSync(imagePath)) {
            const images = readdirSync(imagePath);

            // importing .env while seeding did not work, so path's are hardcoded for now
            const imagePathPublic = `./uploads/final/${mod.id}`; // `${env.UPLOAD_DIR}${modinfo.modid}`;
            const imagePublicUri = `/api/files/${mod.id}`; // `${env.UPLOAD_DIR_URI}${modinfo.modid}`;

            // make sure the folder exists
            mkdirSync(imagePathPublic, { recursive: true });

            for (const image of images) {
                console.log(`[Image] Move image '${image}' to public folder '${imagePathPublic}' (URL: ${`${imagePublicUri}/${image}`})`);
                copyFileSync(`${imagePath}/${image}`, `${imagePathPublic}/${image}`);

                await prisma.image.create({ data: { id: randomUUID(), url: `${imagePublicUri}/${image}`, contentType: 'image/png', fileName: image, modid: mod!.id }});
            }
        }

        // add assset
        const assetPath = `./uploads/final/`;
        const assetUri = `/api/files/${mod.id}mod-data.zip`;

        if (!existsSync(assetPath)) {
            mkdirSync(assetPath, { recursive: true });
        }
        
        const fileName = `mod-data-${modCounter++}.zip`
        const fileLocation = `${assetPath}/${fileName}`;

        copyFileSync(`${dataDir}/mod-data.zip`, fileLocation);
        const asset = await prisma.asset.create({ data: { id: randomUUID(), filelocation: fileName, url: assetUri } });
        await prisma.modHasAsset.create({ data: { modid: mod.id, assetid: asset.id } });
    }
}

// change to seedWithRealData() to seed with scraped data
seedWithDummyData()
    .then(async () => {
        await prisma.$disconnect()
    })
    .catch(async (e) => {
        console.error(e)
        await prisma.$disconnect()
        process.exit(1)
    })