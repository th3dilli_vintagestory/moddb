/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        primary: '#047ac4',
        secondary: '#ca7c0ef1',
        white: '#ffffff',
        purple: '#3f3cbb',
        midnight: '#121063',
        metal: '#565584',
        tahiti: '#3ab7bf',
        silver: '#ecebff',
        bubblegum: '#ff77e9',
        bermuda: '#78dcca',
        card: '#102c44',
        navbar: '#3f3cbb',
        footer: '#3f3cbb'
      }
    },
  },
  plugins: [
    require('@tailwindcss/line-clamp')
  ],
};
