/* eslint-disable */
// Disable ESLint to prevent failing linting inside the Next.js repo.
// If you're using ESLint on your project, we recommend installing the ESLint Cypress plugin instead:
// https://github.com/cypress-io/eslint-plugin-cypress

describe('Navigation', () => {
    it('should navigate to the createMod page', () => {
      // Start from the index page
      cy.visit('http://localhost:3000/createMod');
  
      cy.url().should('include', '/createMod');  
    });


  it('should navigate through icon click', () => {
    // Start from the index page
    cy.visit('http://localhost:3000');
    cy.get('[href="/createMod"]').click()

    cy.url().should('include', '/createMod');  
  });
})

describe('Basic text inserting', () => {
    it('input should have title and description before submitting', () => {
      // Start from the index page
      cy.visit('http://localhost:3000/createMod');
      cy.get('input:invalid').should('have.length', 2)

      cy.log('**enter the item**')
      cy.get('[placeholder="Title"]').type('random Text')
      cy.get('input:invalid').should('have.length', 1)

      cy.log('**enter quantity**')
      cy.get('[placeholder="Description"]').type('random description')
      cy.get('input:invalid').should('have.length', 0)
    });


  it('should not be submittable without title', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/createMod');
    cy.get('button').first().click()

    cy.url().should('include', '/createMod'); 


  });

  it('should redirect to mods page when submitting works', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/createMod');
    cy.get('input:invalid').should('have.length', 2)

      cy.log('**enter the item**')
      const modTitle = makeRandomText(10);
      cy.get('[placeholder="Title"]').type(modTitle)
      cy.get('[placeholder="Description"]').type('random description')
      cy.get('button').first().click()

      cy.url().should('include', '/mods'); 


  });

  it('should have mod with random title in the mods table', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/createMod');
    cy.get('input:invalid').should('have.length', 2)

      cy.log('**enter the item**')
      const modTitle = makeRandomText(10);
      cy.get('[placeholder="Title"]').type(modTitle)
      cy.get('[placeholder="Description"]').type('random description')
      cy.get('button').first().click()

      cy.contains(modTitle); 


  });

  it('should have mod with random description in the mods table', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/createMod');

      cy.log('**enter the item**')
      const modDescription = makeRandomText(10);
      cy.get('[placeholder="Title"]').type('randomTitle')
      cy.get('[placeholder="Description"]').type(modDescription)
      cy.get('button').first().click()

      cy.contains(modDescription); 


  });

  it('should have tags properly inserted', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/createMod');

      cy.log('**enter the item**')
      const modDescription = makeRandomText(10);
      cy.get('[placeholder="Title"]').type('randomTitle')
      cy.get('[placeholder="Description"]').type('randomDesc')
      cy.get('[placeholder="enter tags that describe the mod"]').type(modDescription +  '{enter}')


      cy.get('button').last().click()

      cy.contains(modDescription); 


  });

  it('tags should be deleteable', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/createMod');

      cy.log('**enter the item**')
      const modTag = makeRandomText(10);
      cy.get('[placeholder="Title"]').type('randomTitle')
      cy.get('[placeholder="Description"]').type('randomDesc')
      cy.get('[placeholder="enter tags that describe the mod"]').type(modTag +  '{enter}' + makeRandomText(5) + '{enter}')

    // remove first tag
      cy.get('button').first().click()
      cy.get('button').last().click()

      cy.contains(modTag).should('not.exist'); 


  });

})

function makeRandomText(length: number) {
    let result = '';
     const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
     const charactersLength = characters.length;
     let counter = 0;
     while (counter < length) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
       counter += 1;
     }
     return result;
 }

  // Prevent TypeScript from reading file as legacy script
  export {}
  