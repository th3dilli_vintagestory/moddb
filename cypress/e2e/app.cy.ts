/* eslint-disable */
// Disable ESLint to prevent failing linting inside the Next.js repo.
// If you're using ESLint on your project, we recommend installing the ESLint Cypress plugin instead:
// https://github.com/cypress-io/eslint-plugin-cypress

describe('Navigation', () => {
  it('should navigate to the about page', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/');

    // Find a link with an href attribute containing "about" and click it
    cy.get('a[href*="about"]').click();

    // The new url should include "/about"
    cy.url().should('include', '/about');
  });

  it('about page should include heading', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/');

    // Find a link with an href attribute containing "about" and click it
    cy.get('a[href*="about"]').click();

    // The new page should contain an h1 elements containing "Idea" and "Team"
    cy.get('h1').contains('Idea');
    cy.get('h1').contains('Team');
  });

  it('about page should include team grid with 4 elements', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/');

    // Find a link with an href attribute containing "about" and click it
    cy.get('a[href*="about"]').click();

    // and should include a grid with 4 people
    cy.get('div.grid').children().should('have.length', 4);
  });
})

// Prevent TypeScript from reading file as legacy script
export {}
